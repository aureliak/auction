package pl.auction.services;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.auction.model.Category;
import pl.auction.repository.AuctionRepository;
import pl.auction.repository.CategoryRepository;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
class CategoryServiceTest {
    public static final int ID = 1;
    public static final String MY_CATEGORY = "my category";

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryRepository categoryRepository;

    @After
    public void after() {
        categoryRepository.deleteAll();
    }

    @Before
    public void before() {
        categoryRepository.deleteAll();

    }

    @Test
    void getAllCategoriesTest() {
        //given
        Category category = new Category();
        category.setName(MY_CATEGORY);
        categoryRepository.save(category);
        //when
        List<Category> allCategoriesResult = categoryService.getAllCategories();
        //then
        assertThat(allCategoriesResult).hasSize(1);
        assertThat(allCategoriesResult.get(0).getName()).isEqualTo(MY_CATEGORY);
        assertThat(allCategoriesResult.get(0).getId()).isEqualTo(ID);
    }

    @Test
    void addCategoryTest() {
        //given
        Category category = new Category();
        //when
        categoryService.addCategory(category);
        List<Category> result = categoryRepository.findAll();
        //then
        assertThat(result.get(0).getId()).isEqualTo(ID);
    }
}