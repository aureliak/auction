package pl.auction.services;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import pl.auction.model.Auction;
import pl.auction.model.Advertisement;
import pl.auction.repository.AuctionRepository;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AuctionServicesTest {
    public static final int ID = 1;
    public static final String MY_DESCRIPTION = "my description";
    @Autowired
    AuctionRepository auctionRepository;
    @Autowired
    AuctionServices auctionServices;


    @Test
    @DirtiesContext
    public void saveAuctionTest() {
        //given
        Auction auction = new Auction();
        auction.setDescription(MY_DESCRIPTION);
        //when
        auctionServices.saveAdvertisment(auction);
        List<Advertisement> auctions = auctionRepository.findAll();
        //then
        assertThat(auctions).hasSize(1);
        assertThat(auctions.get(0).getDescription()).isEqualTo(MY_DESCRIPTION);

    }

    @Test
    @DirtiesContext
    public void getListOfCurrentAuctionTest() {
        //given
        Auction auction = new Auction();
        Auction auction2 = new Auction();
        auction.setDescription(MY_DESCRIPTION);
        auction.setDateOfIssue(LocalDate.of(2020, 03, 03));
        auction.setDateOfExpiration(LocalDate.of(2050, 03, 04));
        auction.setIsClosed(false);
        auction2.setDateOfIssue(LocalDate.of(2019, 01, 01));
        auction2.setDateOfExpiration(LocalDate.of(2019, 03, 02));
        auction2.setIsClosed(false);

        auctionRepository.save(auction);
        auctionRepository.save(auction2);
        //when
        List<Advertisement> listOfCurrentAuction = auctionServices.getListOfCurrentAuction();
        //then
        assertThat(listOfCurrentAuction).hasSize(1);
        assertThat(listOfCurrentAuction.get(0).getDescription()).isEqualTo(MY_DESCRIPTION);
    }


    @Test
    @DirtiesContext
    public void getAuctionByIdTest() {
        //given
        Auction auction = new Auction();
        auctionRepository.save(auction);
        //when
        Advertisement searchedAuction = auctionServices.getAuctionById(ID);
        //then
        assertThat(searchedAuction);
        assertThat(searchedAuction).isEqualTo(auction);
    }
}