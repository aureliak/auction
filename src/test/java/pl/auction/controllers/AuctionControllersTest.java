package pl.auction.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.auction.model.Auction;
import pl.auction.repository.AuctionRepository;


import java.math.BigDecimal;
import java.time.LocalDate;



import static org.hamcrest.core.Is.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class AuctionControllersTest {
    public static final String MY_TITLE = "my title";
    public static final String MY_DESCRIPTION = "my description";
    public static final BigDecimal MINIMAL_PRICE = new BigDecimal(123.0);
    public static final int ID = 1;
    @Autowired
    AuctionRepository auctionRepository;
    @Autowired
    AuctionControllers auctionControllers;

    @Autowired
    MockMvc mockMvc;


    @Test
    @DirtiesContext
    public void getListOfCurrentAuctions() throws Exception {
        //given
        Auction auction = new Auction();
        auction.setTitle(MY_TITLE);
        auction.setDescription(MY_DESCRIPTION);
        auction.setMinimalPrice(MINIMAL_PRICE);
        auction.setIsClosed(false);
        auction.setDateOfIssue(LocalDate.of(2020, 01, 01));
        auction.setDateOfExpiration(LocalDate.of(3000, 12, 30));
        auctionRepository.save(auction);
        //then
        mockMvc.perform(get("/auction"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].title", is(MY_TITLE)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].description", is(MY_DESCRIPTION)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].minimalPrice", is(MINIMAL_PRICE.doubleValue())));
    }


    @Test
    @DirtiesContext
    public void createAuction() throws Exception {
        //given
        //language=JSON
        String auction = "{\n" +
                "  \"title\": \"my title\",\n" +
                "  \"description\": \"my description\",\n" +
                "  \"minimalPrice\": 123,\n" +
                "  \"buyNow\": 1999,\n" +
                "  \"isPromoted\": true,\n" +
                "  \"localDate\": \"2020-05-03\",\n" +
                "  \"dateOfIssue\": \"2020-01-01\",\n" +
                "  \"dateOfExpiration\": \"2020-03-05\"\n" +
                "\n" +
                "}";
        //then
        mockMvc.perform(post("/auction", 2)
                .contentType(MediaType.APPLICATION_JSON)
                .content(auction))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title", is(MY_TITLE)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description", is(MY_DESCRIPTION)));
//                .andExpect(MockMvcResultMatchers.jsonPath("$.minimalPrice", is(MINIMAL_PRICE.doubleValue())));

    }

    @Test
    @DirtiesContext
    public void getAuctionById() throws Exception {
        //given
        Auction auction = new Auction();
        auction.setTitle(MY_TITLE);
        auctionRepository.save(auction);
        //then
        mockMvc.perform(get("/auction/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title", is(MY_TITLE)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(ID)));


    }
}