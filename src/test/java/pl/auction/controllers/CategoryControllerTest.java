package pl.auction.controllers;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.auction.model.Category;
import pl.auction.repository.CategoryRepository;

import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
@RunWith(SpringRunner.class)
class CategoryControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void getAllCategories() throws Exception {
        //when
        mockMvc.perform(get("/category", 2))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name", is("car")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].id", is(1)));
    }

    @Test
    void createCategory() throws Exception {
        //given
        //language=JSON
        String json = "{\n" +
                "  \"name\": \"CATEGORY\"\n" +
                "}";
        //then
        mockMvc.perform(post("/category", 2)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", is("CATEGORY")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(3)));
    }
}