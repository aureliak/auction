package pl.auction.controllers;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.auction.model.Advertisement;
import pl.auction.model.Bidding;
import pl.auction.model.Category;
import pl.auction.services.AdvertisementService;
import pl.auction.services.BiddingServices;
import pl.auction.services.CategoryService;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
@RunWith(SpringRunner.class)
class BiddingControllersTest {
    private static final String currentPrice = "100.0";
    @Autowired
    MockMvc mockMvc;
    @Autowired
    BiddingServices advertisementService;
    @Autowired
    CategoryService categoryService;


    @Test
    @DirtiesContext
    void updateCurrentPrice() throws Exception {
        //given
        Bidding bidding = new Bidding();
        advertisementService.saveAdvertisment(bidding);
        //then
        mockMvc.perform(post("/bidding/1?currentPrice=" + currentPrice, 2)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.currentPrice", is(100.0)));
    }

    @Test
    @DirtiesContext
    void createBidding() throws Exception {
        //language=JSON
        String bidding = "{\n" +
                "  \"title\": \"my title\",\n" +
                "  \"description\": \"my description\",\n" +
                "  \"minimalPrice\": 123,\n" +
                "  \"buyNow\": 1999,\n" +
                "  \"isPromoted\": true,\n" +
                "  \"localDate\": \"2020-05-03\",\n" +
                "  \"dateOfIssue\": \"2020-01-01\",\n" +
                "  \"dateOfExpiration\": \"2020-03-05\",\n" +
                "  \"bidding\": true,\n" +
                "  \"closed\": false\n" +
                "  \n" +
                "\n" +
                "}";
        //then
        mockMvc.perform(post("/bidding", 2)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bidding))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.bidding", is(true)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bidding", is(true)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.closed", is(false)));
    }

    @Test
    @DirtiesContext
    void getAuctionById() throws Exception {
        //given
        Bidding bidding = new Bidding();
        bidding.setCurrentPrice(BigDecimal.valueOf(123.7));
        List<Category> allCategories = categoryService.getAllCategories();
        Category category = allCategories.get(0);
        bidding.setCategory(category);
        advertisementService.saveAdvertisment(bidding);
        //then
        mockMvc.perform(get("/bidding/1", 2))
//                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.category.name", is("car")));
//                .andExpect(MockMvcResultMatchers.jsonPath("$.bidding", is(true)))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.closed", is(false)));
    }


}