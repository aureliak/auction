package pl.auction.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index.html");
        registry.addViewController("/createAuction").setViewName("addAuction.html");
        registry.addViewController("/auctions").setViewName("auctionsList.html");
        registry.addViewController("/login").setViewName("login.html");
    }
}
