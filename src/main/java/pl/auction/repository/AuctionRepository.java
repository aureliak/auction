package pl.auction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.auction.model.Advertisement;

import java.util.List;

public interface AuctionRepository extends JpaRepository<Advertisement, Integer> {
    Advertisement findById(int id);


    @Query("select a from Advertisement a where a.dateOfExpiration >= current_date and a.isClosed = 0 ")
     List<Advertisement> getCurrentAuctions();
}
