package pl.auction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.auction.model.Category;

public interface CategoryRepository extends JpaRepository<Category,Integer> {
}
