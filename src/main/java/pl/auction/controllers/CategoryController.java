package pl.auction.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.auction.model.Category;
import pl.auction.services.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/category")
@CrossOrigin("*")
public class CategoryController {
    CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public List<Category> getAllCategories() {
        List<Category> allCategories = categoryService.getAllCategories();
        return allCategories;
    }

    @PostMapping
    public Category createCategory(@RequestBody Category category){
       return categoryService.addCategory(category);
    }


}
