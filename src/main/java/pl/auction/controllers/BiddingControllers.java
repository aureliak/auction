package pl.auction.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.auction.model.Advertisement;
import pl.auction.model.Bidding;
import pl.auction.services.AuctionServices;

import java.math.BigDecimal;

@RestController
@RequestMapping("/bidding")
public class BiddingControllers {
    AuctionServices advertisementService;

    @Autowired
    public BiddingControllers(AuctionServices auctionServices) {
        this.advertisementService = auctionServices;
    }

    @PostMapping
    public Advertisement createBidding(@RequestBody Bidding bidding) {
        bidding.setBidding(true);
        return advertisementService.saveAdvertisment(bidding);
    }

    @GetMapping("/{id}")
    public Advertisement getAuctionById(@PathVariable int id) {
        return advertisementService.getAuctionById(id);
    }

    @PostMapping("/{id}")
    public Advertisement updateCurrentPrice(@PathVariable int id, @RequestParam(name = "currentPrice") String currentPrice) {
        Bidding auctionById = (Bidding) advertisementService.getAuctionById(id);
        auctionById.setCurrentPrice(new BigDecimal(currentPrice));
        advertisementService.saveAdvertisment(auctionById);
        return auctionById;
    }
}
