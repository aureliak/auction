package pl.auction.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.auction.model.Auction;
import pl.auction.model.Advertisement;
import pl.auction.services.AuctionServices;

import java.util.List;


@RequestMapping("/auction")
@RestController("/auction")
@CrossOrigin("*")
public class AuctionControllers {
    AuctionServices auctionServices;

    @Autowired
    public AuctionControllers(AuctionServices auctionServices) {
        this.auctionServices = auctionServices;
    }

    @GetMapping
    public List<Advertisement> getListOfCurrentAuctions() {
        List<Advertisement> currentAuctions = auctionServices.getListOfCurrentAuction();
        return currentAuctions;
    }


    @PostMapping
    public Advertisement createAuction(@RequestBody Auction auction) {
        System.out.println("utworzono aukcje");
        return auctionServices.saveAdvertisment(auction);
    }



    @GetMapping("/{id}")
    public Advertisement getAuctionById(@PathVariable int id) {

        return auctionServices.getAuctionById(id);
    }

    @PostMapping("/change/{id}")
    public Advertisement updateAuctionWhenClickedBuyNow(@PathVariable int id) {
        Advertisement findedAuction = auctionServices.getAuctionById(id);
        findedAuction.setId(id);
        findedAuction.setClosed(true);
        auctionServices.saveAdvertisment(findedAuction);
        return findedAuction;
    }

}


