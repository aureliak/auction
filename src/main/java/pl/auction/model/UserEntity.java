package pl.auction.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static pl.auction.model.Privilage.Admin;

@Data
@RequiredArgsConstructor
@Slf4j
@Entity
@Table(name = "user_entity")
public class UserEntity implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int user_id;

    private String login;
    private String password;
    @Column(name = "account_name")
    private String accountName;
    private String voivodeship;
    private String city;
    private String street;
    @Column(name = "apartment_number")
    private int apartmentNumber;
    @Column(name = "zip_code")
    private String zipCode;

    @Enumerated(EnumType.STRING)
    private Status status;
    @Column(name = "is_premium")
    private boolean isPremium;

    @Enumerated(EnumType.STRING)
    private Privilage privilage;



    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority user = new SimpleGrantedAuthority("ROLE_USER");
        if (Admin.equals(this.privilage)) {
        SimpleGrantedAuthority admin = new SimpleGrantedAuthority("ROLE_ADMIN");
            return Arrays.asList(admin, user); // posiada role Usera i Admina
        } else {
            return Collections.singletonList(user); // w przeciwnym razie tylko usera
        }
    }

    @Override
    public String getPassword() {
        return null;
    }

    public String getLogin() {
        return login;
    }

    public UserEntity setPrivilage(Privilage privilage) {
        this.privilage = privilage;
        return this;
    }

    public UserEntity setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
