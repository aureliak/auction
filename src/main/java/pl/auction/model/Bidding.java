package pl.auction.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
public class Bidding extends Advertisement {

    private BigDecimal currentPrice;
    @Column(name="type", insertable = false, updatable = false)
    private String type;
    //    @OneToOne
//    private UserEntity user;
    public Bidding() {
    }

    public String getType() {
        return type;
    }

    public Bidding setType(String type) {
        this.type = type;
        return this;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }
}
