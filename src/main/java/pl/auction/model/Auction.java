package pl.auction.model;

import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;

@Data
@Entity
@EntityScan
public class Auction extends Advertisement {
    @Column(name = "type", insertable = false, updatable = false)
    private String type;

    public Auction() {
    }


}

