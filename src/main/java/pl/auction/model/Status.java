package pl.auction.model;

public enum Status {
    ACTIVE, NOT_ACTIVE, BLOCKED;
}
