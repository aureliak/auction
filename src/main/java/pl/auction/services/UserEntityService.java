package pl.auction.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.auction.configuration.PasswordEncoderConfiguration;
import pl.auction.model.UserEntity;
import pl.auction.repository.UserEntityRepository;

import java.util.Optional;

import static pl.auction.model.Privilage.User;

@Service
public class UserEntityService implements UserDetailsService {

    private UserEntityRepository userEntityRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserEntityService(UserEntityRepository userEntityRepository, PasswordEncoder passwordEncoder) {
        this.userEntityRepository = userEntityRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void save(UserEntity userEntity){
        String password = passwordEncoder.encode(userEntity.getPassword());
        userEntity.setPassword(password);
        userEntity.setPrivilage(User);
        userEntityRepository.save(userEntity);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> userEntity = userEntityRepository.findUserEntityByAccountName(username);
        if(userEntity.isPresent()) {
            return userEntity.get();
        } else {
            throw new UsernameNotFoundException("User not found!");
        }
    }
}
