package pl.auction.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import pl.auction.model.Advertisement;
import pl.auction.repository.AuctionRepository;

import java.util.List;

public abstract class AdvertisementService {
    AuctionRepository auctionRepository;

    @Autowired
    public AdvertisementService(AuctionRepository auctionRepository) {
        this.auctionRepository = auctionRepository;
    }


    public List<Advertisement> getListOfCurrentAuction() {
        List<Advertisement> currentAuctions = auctionRepository.getCurrentAuctions();
        return currentAuctions;
    }

@Transactional
    public Advertisement saveAdvertisment(Advertisement auction) {
        return auctionRepository.save(auction);
    }


    public Advertisement getAuctionById(int id) {
        return auctionRepository.findById(id);
    }
}
