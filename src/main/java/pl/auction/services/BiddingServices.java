package pl.auction.services;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import pl.auction.repository.AuctionRepository;

@Service
@CrossOrigin("*")
public class BiddingServices extends AdvertisementService {
    public BiddingServices(AuctionRepository auctionRepository) {
        super(auctionRepository);
    }
}
