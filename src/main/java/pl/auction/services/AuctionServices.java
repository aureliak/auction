package pl.auction.services;


import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import pl.auction.repository.AuctionRepository;


@Service
@CrossOrigin("*")
public class AuctionServices extends AdvertisementService{

    public AuctionServices(AuctionRepository auctionRepository) {
        super(auctionRepository);
    }

}
