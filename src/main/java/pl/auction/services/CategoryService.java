package pl.auction.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import pl.auction.model.Category;
import pl.auction.repository.CategoryRepository;

import java.util.List;

@Service
@CrossOrigin("*")
public class CategoryService {
    CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> getAllCategories() {
        List<Category> categories = categoryRepository.findAll();
        return categories;
    }

    @Transactional
    public Category addCategory(Category category) {
        return categoryRepository.save(category);
    }
}
