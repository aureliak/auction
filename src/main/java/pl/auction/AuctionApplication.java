package pl.auction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;


@SpringBootApplication(scanBasePackages = {"pl.auction"})
@PropertySource("classpath:custom.properties")
public class AuctionApplication {


    public static void main(String[] args) {
        SpringApplication.run(AuctionApplication.class, args);
    }


}
