drop schema if exists `auction`;
create schema `auction`;
use `auction`;
drop table if exists `auction`;
SET character_set_client = utf8mb4;
use `auction`;

CREATE TABLE `category`
(
    `id`       int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(45),
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_as_cs;


CREATE TABLE `auction_class`
(
    `id`                 int(11) NOT NULL AUTO_INCREMENT,
    `title`              varchar(45),
    `description`        varchar(45),
    `category_id`           int,
    `minimal_price`      DECIMAL(19, 2),
    `buy_now`            DECIMAL(19, 2),
    `is_promoted`        boolean,
    `is_closed`        boolean,
    `date_of_expiration` DATE,
    `date_of_issue`      DATE,
    `visits`             int,
    PRIMARY KEY (`id`),
    CONSTRAINT FOREIGN KEY (`category_id`) REFERENCES category (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_as_cs;

CREATE TABLE `user_entity`
(

    `id` int(11) NOT NULL AUTO_INCREMENT,
    `login` varchar(45),
    `password` varchar(45),
    `account_name` varchar(45),
    `voivodeship` varchar(45),
    `city` varchar(45),
    `street` varchar(45),
    `apartment_number` int(11) DEFAULT NULL,
    `zip_code` varchar(45),
    `status` enum('ACTIVE', 'NOT_ACTIVE', 'BLOCKED'),
    `privilage` enum('User', 'Admin'),
    `is_premium` boolean,
    PRIMARY KEY (`id`)

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_as_cs;


