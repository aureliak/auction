// fetch("http://localhost:8080",visits++);


let description = document.querySelector("#description");
let title = document.querySelector("#title");
let minimalPrice = document.querySelector("#minimalPrice");
let buyNow = document.querySelector("#buyNow");
let dateOfExpiration = document.querySelector("#dateOfExpiration");
let yesRadio = document.querySelector("#YES");
let noRadio = document.querySelector("#NO");
let auctionRadio = document.querySelector("#auction");
let bidding = document.querySelector("#bidding");
let dateOfIssue = document.querySelector("#dateOfIssue");
// let visits = 0;
let form = document.querySelector("form");

function getCurrentDate(additionalDays) {
    let today = new Date();
    let dd = today.getDate() + additionalDays;
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    return yyyy + '-' + mm + '-' + dd;
}

dateOfIssue.setAttribute("min", getCurrentDate(0));
dateOfExpiration.setAttribute("min", getCurrentDate(7));


form.addEventListener("submit", function (event) {

        event.preventDefault();
        let titleValue = title.value;
        let descriptionValue = description.value;
        let minimalPriceValue = minimalPrice.value;
        let buyNowValue = buyNow.value;
        let isPromoted = getCheckedRadio(yesRadio.checked, noRadio.checked);
        let dateOfExpirationValue = dateOfExpiration.value;
        let dateOfIssueValue = dateOfIssue.value;
        const auction = {
            "title": titleValue,
            "description": descriptionValue,
            "minimalPrice": minimalPriceValue,
            "buyNow": buyNowValue,
            "promoted": isPromoted,
            "isClosed": false,
            "category": {
                "id": getSelectedOption().value,
                "name": getSelectedOption().text
            },
            "dateOfIssue": dateOfIssueValue,
            "dateOfExpiration": dateOfExpirationValue,
        };

        if (auctionRadio.checked) {


            fetch("/auction", {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(auction)
            })
                .then(res => res.json())
                .then(res => {
                    console.log(res);
                    window.location.replace("/auctions")
                })
        } else {
            fetch("/bidding", {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(auction)
            })
                .then(res => res.json())
                .then(res => {
                    // getCategories();
                    console.log("Dodałam licytacje:");
                    console.log(res);
                    window.location.replace("/auctions")
                })
        }
    }
);


function getCheckedRadio(radio1, radio2) {
    if (radio1) {
        return true
    }
    if (radio2) {
        return false
    }

}


//categories2
let select = document.getElementById('mySelect');
select.length = 0;
let option = document.createElement('option');
option.text = `Choose category`;
select.add(option);
select.selectedIndex = 0;

fetch("/category")
    .then(
        function (response) {
            if (response.status !== 200) {
                console.log('something goes wrong');
                response.status;
                return;
            }
            response.json()
                .then(
                    function (categoryList) {
                        let option;

                        for (let i = 0; i < categoryList.length; i++) {
                            option = document.createElement('option');
                            option.text = categoryList[i].name;
                            option.value = categoryList[i].id;
                            select.add(option);
                        }
                    });
        }
    )
    .catch(
        function (err) {
            console.error('Fetch error', err)

        }
    );


function getSelectedOption() {
    let selectedOption = select.options[select.selectedIndex];
    console.log(selectedOption.value);
    console.log(selectedOption.text);
    return selectedOption;

}
