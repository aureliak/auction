let biddingId = getQueryVariable("id");
let form = document.querySelector("form");

function getPrice() {
    let newPrice = document.getElementById("currentPrice");
    if (newPrice != null) {
        return newPrice.value;
    } else {
        return null;
    }
}



function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return (false);
}

fetch("bidding/" + biddingId)
    .then(
        response => response.json())
    .then(response =>
        getBidding(response)
    );

function getBidding(bidding) {
    form.innerHTML = `
            <a>Description ${bidding.description}</a><br>
            <a> Minimal Price ${bidding.minimalPrice}</a><br>
            <a>Buy Now ${bidding.buyNow}</a><br>
            <a>Is Promoted ${bidding.promoted}</a><br>
            <a>Date Of Issue ${bidding.dateOfIssue}</a><br>
            <a>Date Of Expiration ${bidding.dateOfExpiration}</a><br>
            <a>type ${bidding.dtype}</a><br>
            <a>Visits ${bidding.visits}</a><br>
            <input type="text" id="currentPrice" placeholder="New Price">
            <input onclick="getPrice()" id="buyNow" type="submit" value="Bid">`;
    return form;
}

form.addEventListener("submit", function (event) {
    event.preventDefault();
    const auction = {
        "id": biddingId,
    };
    fetch("bidding/" + biddingId + "?currentPrice=" + getPrice(), {
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(auction)
    })
        .then(res => res.json())
});