let form = document.querySelector("form");
let buyNowButton = document.getElementById("buyNow");
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return (false);
}

let id = getQueryVariable("id");


fetch("auction/" + id)
    .then(
        response => response.json())
    .then(response =>
        getAuction(response)
    );

function getAuction(auction) {
    form.innerHTML = `
            <a>Description ${auction.description}</a><br>
            <a> Minimal Price ${auction.minimalPrice}</a><br>
            <a>Buy Now ${auction.buyNow}</a><br>
            <a>Is Promoted ${auction.promoted}</a><br>
            <a>Date Of Issue ${auction.dateOfIssue}</a><br>
            <a>Date Of Expiration ${auction.dateOfExpiration}</a><br>
            <a>type ${auction.dtype}</a><br>
            <a>Visits ${auction.visits}</a><br>
            <input id="buyNow" type="submit" value="Buy Now">`;
    return form;
}

form.addEventListener("submit", function (event) {
    event.preventDefault();
    const auction = {
        "id": id,
    };
    fetch("auction/change/" + getQueryVariable("id"), {
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(auction)
    })
        .then(res => res.json())

        // .then(res => {
        //     window.location.replace("/auctions")
        // })
});