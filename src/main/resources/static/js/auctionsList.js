const tableBody = document.querySelector("tbody");

function createRow(auction) {
    const tableRow = document.createElement("tr");
    tableRow.innerHTML = `
            <td>${auction.id}</td>
            <td><a id="url" onclick="getUrl(${auction.id},${auction.bidding})" href="">${auction.title}</a></td>
            <td>${auction.description}</td>
            <td>${auction.minimalPrice}</td>
            <td>${auction.buyNow}</td>
            <td>${auction.promoted}</td>
            <td>${auction.dateOfIssue}</td>
            <td>${auction.type}</td>
            <td>${auction.dateOfExpiration}</td>
            <td>${auction.visits}</td>`;
    return tableRow;
}


function getUrl(id, type) {
    if (type == false) {
        document.getElementById("url").href = "/particularAuction.html?id=" + id;
    } else {
        document.getElementById("url").href = "/particularBidding.html?id=" + id;
    }
}


function fillTable(auctionsList) {
    tableBody.innerHTML = "";
    auctionsList.forEach(auction => {
        const tableRow = createRow(auction);
        tableBody.appendChild(tableRow);
    });
}

function loadTableData() {
    fetch("/auction")
        .then(response => response.json())
        .then(response => fillTable(response));
}


loadTableData();
